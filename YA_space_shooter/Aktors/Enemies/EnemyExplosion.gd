extends CPUParticles2D

func _ready() -> void:
	# start explosion
	emitting = true
	# shake camera
	var camShake := get_tree().current_scene.find_node("CamShake", true, false)
	camShake.shake(5)


func _process(_delta: float) -> void:
	if ! emitting:
		queue_free()
