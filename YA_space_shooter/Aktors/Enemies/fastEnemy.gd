extends Enemy

export var rotationRate := 20

func _process(delta: float) -> void:
	rotate(deg2rad(rotationRate) * delta)

