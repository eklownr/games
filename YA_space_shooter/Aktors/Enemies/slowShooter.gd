extends Enemy

onready var fireTimer := $FireTimer
export var fireRate := 1.0


func _process(_delta: float) -> void:
	if fireTimer.is_stopped():
		shooting()
		fireTimer.start(fireRate)

