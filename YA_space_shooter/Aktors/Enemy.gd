extends Area2D
class_name Enemy

export var scoreRate := 1
export var hp := 15
export var speed := 50.0
export var spread: int  = rand_range(30, 300) 
var plBullet := load("res://Bullet/EnemyBullet.tscn")
var plEnemyExplosion := preload("res://Aktors/Enemies/EnemyExplosion.tscn")
var velocity :=  Vector2.ZERO
var dir = 1

onready var firePos := $FiringPosition


func _physics_process(delta: float) -> void:
	position.y += speed * delta
	
	# move enemy in x position
	if dir <= spread :
		position.x += 1
		dir += 1
	elif dir <= (spread * 2):
		position.x -= 1
		dir += 1
	elif dir > spread*2:
		dir = 1	


func take_damage(amount: int):
	# make shore score only adds up once
	if hp <= 0:
		return
		
	hp -= amount
	if hp <= 0:
		var effect := plEnemyExplosion.instance()
		effect.position = global_position
		get_tree().current_scene.add_child(effect)
		
		Signals.emit_signal("on_score_increment", scoreRate)
		
		queue_free()


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


func _on_Enemy_body_entered(body: Node) -> void:
	if body.is_in_group("player"):
		body.take_damage(1)


func shooting():
	for child in firePos.get_children():
		var bullet = plBullet.instance()
		bullet.global_position = child.global_position
		get_tree().current_scene.add_child(bullet)
