extends KinematicBody2D
class_name Player

var plGameOver := preload("res://HUD/GameOver.tscn")
var plBullet := preload("res://Bullet/Bullet.tscn")
var velocity := Vector2.ZERO
export var speed := 20000.0
export var hp := 3
export var damageInvicTime := 1.5
onready var firePos := $FirePos 
onready var animatedSprite := $AnimatedSprite 
onready var shild = $shildSprite
onready var invincTimer = $invincTimer
onready var shootTimer := $ShootTimer
onready var rapidFireTimer := $RapidFireTimer
onready var treeShootTimer := $TreeShootTimer

var fire_delay := 0.25
export var normal_fire_delay := 0.25
export var rapid_fire_delay := 0.15

var timeout := false
var shied_is_on := false
var treeShoot := false


func _ready() -> void:
	shild.visible = false
	Signals.emit_signal("on_Player_HP_change", hp)


func _process(_delta: float) -> void:
	# Animate player ship
	if velocity.x < 0:
		animatedSprite.play("left")
	elif velocity.x > 0:
		animatedSprite.play("right")
	else:
		animatedSprite.play("straight")
		
	# Check if shooting
	if Input.is_action_pressed("shoot") and shootTimer.is_stopped():
		shootTimer.start(fire_delay)
		if treeShoot == true:
			for child in firePos.get_children():
				var bullet := plBullet.instance()
				bullet.global_position = child.global_position
				get_tree().current_scene.add_child(bullet)
		else: 
			firePos.get_child(2).visible = false
			var bulletChild = firePos.get_child(2)
			var bulletCenter := plBullet.instance()
			bulletCenter.global_position = bulletChild.global_position
			get_tree().current_scene.add_child(bulletCenter)


func _physics_process(delta: float) -> void:
	var direction := Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	)
	# set diagonal direction to phytagoras 
	if direction.x > 0 and direction.y > 0:
		direction.x = 0.77
		direction.y = 0.77
	if direction.x < 0 and direction.y < 0:
		direction.x = -0.77
		direction.y = -0.77
	
	velocity = speed * direction * delta
	velocity = move_and_slide(velocity)
	
	var viewport = get_viewport_rect()
	position.x = clamp(position.x, 0, viewport.size.x)
	position.y = clamp(position.y, 0, viewport.size.y)
	

func take_damage(amount: int):
	if timeout == true or shied_is_on == true:
		return
		
	applayShield(damageInvicTime)
	
	timeout = true
	hp -= amount
	Signals.emit_signal("on_Player_HP_change", hp)
	# shake camera
	var camShake := get_tree().current_scene.find_node("CamShake", true, false)
	camShake.shake(10)
	# Add GAME OVER when Player die
	if hp <= 0:
		var gameOver = plGameOver.instance()
		get_tree().current_scene.add_child(gameOver)
		queue_free()


func applayShield(time: float):
	invincTimer.start(time + invincTimer.time_left)
	shild.visible = true 
	shied_is_on = true


func applayRapidFire(time: float):
	fire_delay = rapid_fire_delay
	rapidFireTimer.start(time + rapidFireTimer.time_left)


func applayTreeBullet(time: float):
	treeShootTimer.start(time + treeShootTimer.time_left)
	treeShoot = true
	#print("tree bullet applyed...")


func _on_invincTimer_timeout() -> void:
	shild.visible = false
	timeout = false
	shied_is_on = false


func _on_RapidFireTimer_timeout() -> void:
	fire_delay = normal_fire_delay


func _on_TreeShootTimer_timeout() -> void:
	treeShoot = false
