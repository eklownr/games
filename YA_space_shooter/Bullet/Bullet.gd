extends Area2D

export var speed := 800.0
var plBulleEffect = preload("res://Bullet/BullerEfect.tscn")


func _physics_process(delta: float) -> void:
	position.y -= speed * delta


func _on__Bullet_area_entered(area: Area2D) -> void:
	if area is Enemy or Meteor:
		var bulletEffect = plBulleEffect.instance()
		bulletEffect.position = position
		get_parent().add_child(bulletEffect)
		area.take_damage(1)
		queue_free()



func take_damage(_amonut: int):
	pass


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
