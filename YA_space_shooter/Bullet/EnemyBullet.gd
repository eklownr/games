extends Area2D

export var speed := 400.0
var plBulleEffect = preload("res://Bullet/BullerEfect.tscn")


func _physics_process(delta: float) -> void:
	position.y += speed * delta


func take_damage(_amonut: int):
	pass


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


func _on_EnemyBullet_body_entered(body: Node) -> void:
	var bulletEffect = plBulleEffect.instance()
	bulletEffect.position = position
	get_parent().add_child(bulletEffect)
	body.take_damage(1)
	queue_free()


func _on_EnemyBullet_area_entered(area: Area2D) -> void:
	# if EnemyBullet enter adder Enemy or Meteor -> do nothing!
	pass 
