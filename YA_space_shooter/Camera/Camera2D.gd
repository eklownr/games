extends Camera2D

export var shakeBaseAmount := 1.0
export var shakeDumpAmount := 0.07


var shakeAmount := 0.0

func _process(_delta: float) -> void:
	if shakeAmount > 0:
		position.x = rand_range(-shakeBaseAmount, shakeBaseAmount) * shakeAmount
		position.y = rand_range(-shakeBaseAmount, shakeBaseAmount) * shakeAmount
		shakeAmount = lerp(shakeAmount, 0.0, shakeDumpAmount)
	else:
		position = Vector2.ZERO


func shake(magnitude: float):
	shakeAmount = magnitude  
	
	
