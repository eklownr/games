extends Control


var score: int = 0
var plHPicon := preload("res://HUD/HPicon.tscn")
onready var hp_container := $hpContainer
onready var scoreLable := $Score 

func _ready() -> void:
	clearHP()
	Signals.connect("on_Player_HP_change", self, "_on_Player_HP_change")
	Signals.connect("on_score_increment", self, "_on_score_increment")

func clearHP():
	for child in hp_container.get_children():
		hp_container.remove_child(child)
		child.queue_free()


func setHP(hp: int):
	clearHP()
	for i in range(hp):
		hp_container.add_child(plHPicon.instance())

func _on_Player_HP_change(hp: int):
	setHP(hp)
		

func _on_score_increment(amount: int):
	score += amount
	scoreLable.text = str(score)
