extends Node

onready var timer := $Timer
onready var anim := $AnimatedSprite

func _ready() -> void:
	timer.start(4)
	
 

func _on_Timer_timeout() -> void:
	anim.visible = false
