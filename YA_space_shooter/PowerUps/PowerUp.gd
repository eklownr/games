extends Area2D
class_name Powerup

export var moveDown := 50.0


func _physics_process(delta: float) -> void:
	position.y += moveDown * delta


func apply_powerup(player: Player):
	# This need to be implemented by the inheriting class
	pass


func _on_PowerUp_body_entered(body: Node) -> void:
	# shake camera
	var camShake := get_tree().current_scene.find_node("CamShake", true, false)
	camShake.shake(10)
	apply_powerup(body)
	queue_free()


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
	
	
func take_damage(_amount: int):
	pass
