# YA_space_shooter
simple_space_shooter

Music from my heavy metal group.
Recorded in my garage, one take, just playing and having fun!
https://www.youtube.com/watch?v=9v1ZAsXHt7Q&t=0s


See the game here:
https://www.youtube.com/watch?v=5iDm1VQGeoE

Play online here:
https://eklow.itch.io/



### Requirement
Godot Engine v3.x

### Installing as a Project
After downloading, open Godot Engine Project Manager. Click Import, go to the folder you've downloaded, and select "project.godot".


