extends Node2D

var preloadedEnemies := [
	preload("res://Aktors/Enemy.tscn"),
	preload("res://Aktors/Enemies/fastEnemy.tscn"),
	preload("res://Aktors/Enemies/slowShooter.tscn"),
	preload("res://Aktors/Enemies/fastEnemy2.tscn")
]

var preloadPowerup := [
	preload("res://PowerUps/PowerupShield.tscn"),
	preload("res://PowerUps/rapidFirePowerup.tscn"),
	preload("res://PowerUps/TreShootPowerup.tscn")
]

var plMeteor := preload("res://meteor/Meteor.tscn")

onready var spawnTimer := $SpawnTimer
onready var powerupTimer := $PowerupTimer
export var nexEnemySpawn := 3.0
export var minPowerupSpawnTime := 2
export var maxPowerupSpawnTime := 15



func _ready() -> void:
	randomize()
	spawnTimer.start(nexEnemySpawn)
	powerupTimer.start(minPowerupSpawnTime)


func getRandomSpawnPos() -> Vector2:
	var viewRect := get_viewport_rect()
	var xPos := rand_range(viewRect.position.x, viewRect.end.x)
	return Vector2(xPos, position.y)


func _on_SpawnTimer_timeout() -> void:
	
	# add meteor 30% and 70% an enemy
	if randf() < 0.3:
		var meteor: Meteor = plMeteor.instance()
		meteor.position = getRandomSpawnPos()
		get_tree().current_scene.add_child(meteor)
	
	var preloadEnemy = preloadedEnemies[randi() % preloadedEnemies.size()]
	var enemy: Enemy = preloadEnemy.instance()
	enemy.position = getRandomSpawnPos()
	# add Enemy to scene
	get_tree().current_scene.add_child(enemy)
	
	# Restart the timer
	spawnTimer.start(nexEnemySpawn)
	# make it more enemy every time spawnTimer starts
	nexEnemySpawn -= 0.05
	if nexEnemySpawn < 1.2:
		nexEnemySpawn = 1.2 


func _on_PowerupTimer_timeout() -> void:
	# add the preloaded powerup, randomly from the arroy
	var powerup_preload = preloadPowerup[randi() % preloadPowerup.size()]
	var powerup: Powerup = powerup_preload.instance()
	powerup.position = getRandomSpawnPos()
	get_tree().current_scene.add_child(powerup)
	powerupTimer.start(rand_range(minPowerupSpawnTime, maxPowerupSpawnTime))
	
	
