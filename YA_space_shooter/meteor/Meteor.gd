extends Area2D
class_name Meteor

var plMeteorEffect := preload("res://meteor/MeteorEffect.tscn")
export var hp := 20
export var min_speed := 10.0
export var max_speed := 20.0
var speed := 0.0

export var max_spin: float = -0.1
export var min_spin: float = 0.1
var spin: float =rand_range(min_spin, max_spin)




func _ready() -> void:
	speed = rand_range(min_speed, max_speed)
	

func _physics_process(delta: float) -> void:
	position.y += speed * delta
	rotate(spin * delta)


func take_damage(amount: int):
	# make shore score only adds up once
	if hp <= 0:
		return
		
	hp -= amount
	if hp <= 0:
		var effect := plMeteorEffect.instance()
		effect.position = position
		get_parent().add_child(effect)
		# shake camera
		var camShake := get_tree().current_scene.find_node("CamShake", true, false)
		camShake.shake(5)
		# Give score to Player
		Signals.emit_signal("on_score_increment", 10)
		queue_free()


func _on_Meteor_body_shape_entered(_body_id: int, body: Node, _body_shape: int, _area_shape: int) -> void:
	if body.is_in_group("player"):
		body.take_damage(1)


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


