import pygame
from sys import exit
from random import randint, choice


class Player(pygame.sprite.Sprite):
    def __init__(self) -> None:
        super().__init__()
        # load images
        self.player_jump = pygame.image.load("images/player/player_jump.png").convert_alpha()
        player_walk_1 = pygame.image.load("images/player/player_walk_1.png").convert_alpha()
        player_walk_2 = pygame.image.load("images/player/player_walk_2.png").convert_alpha()
        self.player_walk_right = [player_walk_1, player_walk_2]
        # flip images to walk left
        player_walk_3 = pygame.transform.flip(player_walk_1, True, False)
        player_walk_4 = pygame.transform.flip(player_walk_2, True, False)
        self.player_walk_left = [player_walk_3, player_walk_4]
        
        self.player_index = 0
        self.image = self.player_walk_right[self.player_index]
        self.rect = self.image.get_rect(midbottom = (200,310))
        self.gravity = 0

        self.jump_sound = pygame.mixer.Sound("audio/jump.mp3")

    def player_input(self):
        # jump
        key = pygame.key.get_pressed()
        if key[pygame.K_SPACE] or key[pygame.K_UP]:
            if self.rect.bottom >= 310:
                self.gravity = -20
                self.player_jump
                self.animation("jump")
                self.jump_sound.play().set_volume(0.2)

    def apply_gravity(self):
        self.gravity += 1.5
        self.rect.y += self.gravity  
        if self.rect.bottom >= 310:
            self.rect.bottom = 310


    def player_move(self):
        key = pygame.key.get_pressed()
        if key [pygame.K_RIGHT]:
            self.rect.x += 10
            self.animation("right")
        if key [pygame.K_LEFT]:
            self.rect.x -= 10
            self.animation("left")
        if key [pygame.K_DOWN]:
            self.rect.y += 10
            self.animation("jump")


    def animation(self, direction):
        self.player_index += 0.1
        if self.player_index >= len(self.player_walk_right): 
            self.player_index = 0     

        if direction == "right":
            self.image = self.player_walk_right[int(self.player_index)]
        if direction == "left":
            self.image = self.player_walk_left[int(self.player_index)]
        if direction == "jump":
            self.image = self.player_jump


    def update(self):
        self.player_input()
        self.apply_gravity()
        self.player_move()


class Enemy(pygame.sprite.Sprite):
    def __init__(self, type):
        super().__init__()
        
        if type == "fly":
            fly_frame1 = pygame.image.load("images/enemy/fly1.png").convert_alpha()
            fly_frame2 = pygame.image.load("images/enemy/fly2.png").convert_alpha()
            self.frames = [fly_frame1, fly_frame2]
            pos_y = 230

        elif type == "snail":
            snail_frame1 = pygame.image.load("images/enemy/snail1.png").convert_alpha()
            snail_frame2 = pygame.image.load("images/enemy/snail2.png").convert_alpha()
            self.frames = [snail_frame1, snail_frame2]
            pos_y = 310

        self.animation_index = 0
        self.image = self.frames[self.animation_index]
        self.rect = self.image.get_rect(midbottom = (randint(900, 1000), pos_y))

    def animation(self):
        self.animation_index += 0.1
        if self.animation_index >= len(self.frames): 
            self.animation_index = 0
        self.image = self.frames[int(self.animation_index)]

    def sprite_killer(self):
        if self.rect.x <= -100:
            self.kill()

    def update(self):
        self.animation()
        self.rect.x -= 6
        self.sprite_killer()


def display_score():
    current_time = int(pygame.time.get_ticks() / 1000) - score_time
    score_surface = test_font.render(f'Score: {current_time}', True, "Green")
    score_rect = score_surface.get_rect(center = (400, 50))
    screen.blit(score_surface, score_rect)
    return current_time


def collision_sprite():
    if pygame.sprite.spritecollide(player_group.sprite, enemy_group,True):
        enemy_group.empty()
        return False
    else:
        return True


pygame.init()
screen = pygame.display.set_mode((800, 400))
pygame.display.set_caption("Baby Runner")
clock = pygame.time.Clock()
test_font = pygame.font.Font("m_bold.otf", 20)
game_active = False
score_time = 0
score = 0

# Add music
background_sound = pygame.mixer.Sound("audio/music.wav")
background_sound.play(loops=-1).set_volume(0.5) 
                
### Groups ###
player_group = pygame.sprite.GroupSingle()
player_group.add(Player())
enemy_group = pygame.sprite.Group() # enemy_group.add in the Game-loop
    

### Load sky and ground images
sky_surface = pygame.image.load("images/sky.png").convert_alpha()
ground_surface = pygame.image.load("images/ground.png").convert_alpha()


### Intro sceen
player_stand = pygame.image.load("images/player/player_stand_2.png").convert_alpha()
player_stand_scaled = pygame.transform.scale2x(player_stand)
player_stand_rect = player_stand_scaled.get_rect(center = (400, 150))

game_name = test_font.render("Baby Runner", True, (100,200,50))
game_name_rect = game_name.get_rect(center = (400, 20)) 

game_message = test_font.render("Press space to run --- esc to quit" , True,(100,200,50))
game_message_rect = game_message.get_rect(center = (400, 250))


### Timer
enemy_timer = pygame.USEREVENT + 2
pygame.time.set_timer(enemy_timer, 1500)


### game loop ###
while True:
    # check for event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        ### timer for Enemy obsticles
        if game_active:
            if event.type == enemy_timer:
                enemy_group.add(Enemy(choice(["fly", "snail", "snail"])))
  
        # Game not active
        else:
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                game_active = True
                #snail_rect.x = 800
                score_time = int(pygame.time.get_ticks() / 1000)
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                pygame.quit()
                exit()

    if game_active:   
        
        # Add surfacies to the screen
        screen.blit(sky_surface, (0,0))
        screen.blit(ground_surface, (0,300))
     
        # from class Player:
        player_group.draw(screen)
        player_group.update() 
        # from class Enemy
        enemy_group.draw(screen)
        enemy_group.update()

        # check for collision
        game_active = collision_sprite()

        score = display_score()

    else:
        ### Intro sceen
        screen.fill((94,129,162))
        screen.blit(player_stand_scaled, player_stand_rect)
        screen.blit(game_name, game_name_rect)
        screen.blit(game_message, game_message_rect)
        
        score_message = test_font.render(f'Score: {score}', True, "Green")
        score_message_rect = score_message.get_rect(center = (400, 50))
        screen.blit(score_message, score_message_rect)

    pygame.display.update()
    clock.tick(30) 
